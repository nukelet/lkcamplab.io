---
title: "Apresentação de Projetos"
date: 2023-05-26T20:46:20-03:00
draft: false
categories: ["Evento"]
tags: ["GNOME", "Tradução", "Projetos internos", "Open Source Hardware"]
authors: ["Pedro Sader Azevedo", "Julio Nunes Avelar", "Tárik Sá"]
---

# GNOME

O GNOME é um projeto que desenvolve uma série de tecnologias para o desktop
GNU/Linux, sendo o ambiente gráfico (*desktop environment*) GNOME Shell a mais
emblemática entre elas. Além disso, o GNOME mantém uma série de bibliotecas e
ferramentas de desenvolvimento de software, assim como um vasto ecossistema de
aplicativos.

**Tecnologias**: GTK, C, Javascript, Python, Rust

**Modelo de contribuição**: [GitLab](gitlab.gnome.org/)

**Comunicação do projeto**: [Matrix](https://wiki.gnome.org/GettingInTouch/Matrix/ExploringGnomeDirectory), [Discourse](https://discourse.gnome.org/)

**Tópico no Telegram do LKCAMP**: [\[GA\] GNOME](https://t.me/lkcamp/15257)

**Mentores do LKCAMP**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Gustavo Montenari Pechta ([@GuPecz](https://t.me/GuPecz))

---

# Hardware Livre


Hardware livre é um termo que descreve o desenvolvimento de hardware com licenças abertas, que permitem o estudo, modificação, fabricação e distribuição livre do mesmo. Essa abordagem é de extrema importância por diversos motivos: promove a transparência, concede liberdade ao usuário, aumenta a segurança e contribui para a sustentabilidade.

A possibilidade de compreender e personalizar dispositivos é um dos grandes atrativos do hardware livre, pois estimula a inovação e o desenvolvimento colaborativo. Através da utilização de hardware de código aberto, é possível alcançar a verdadeira liberdade, complementando o uso de software livre.

**Tecnologias**: C, C++, Python, Rust, VHDL, Eletronica

**Comunicação do projeto**: Depende do projeto

**Tópico no Telegram do LKCAMP**: [\[GA\] Hardware Livre](https://t.me/lkcamp/15257)

**Mentores do LKCAMP**:

- Julio Nunes Avelar ([@jn513](https://t.me/jn513))
- Gabriel Lima Luz ([@Rai0Catodic0](https://t.me/Rai0Catodic0))
- Carlos Eduardo C. Barbosa ([@CarlosRFS](https://t.me/CarlosRFS))
- Mariana Heimbecher Arias ([@MarianaBzoid](https://t.me/MarianaBzoid))

---

# Tradução

A tradução é essencial para ampliar o acesso à tecnologia, sendo fundamental
para garantir a "liberdade zero" do Software Livre: uso para qualquer
propósito. Além disso, contribuir com traduções pode ser menos intimidador
que contribuir com programação, por isso é uma excelente porta de entrada no
mundo FOSS.


**Tecnologias**: gettext, make, tecnologias web

**Modelo de contribuição**: Portais web (Weblate, DDTSS, etc), git forges

**Comunicação do projeto**: Depende do projeto traduzido

**Tópico no Telegram do LKCAMP**: [\[GA\] Tradução](https://t.me/lkcamp/15259)

**Mentores**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Charles Melara ([@charles_melara](https://t.me/charles_melara))

---

# Projetos internos

O LKCAMP mantém uma série de projetos de código aberto, utilizados não apenas
por si mesmo mas também para outras entidades estudantis da Unicamp. Alguns
desses projetos são nosso site, nosso bot do telegram, e nossa ferramenta de
geração de certificados de participação em eventos.

**Tecnologias**: Hugo, Markdown, Python, GitLab CI

**Modelo de contribuição**: [GitLab](https://gitlab.com/lkcamp)

**Comunicação do projeto**: Grupo de Telegram do LKCAMP

**Tópico no Telegram do LKCAMP**: [\[GT\] Nils](https://t.me/lkcamp/15280), [Site](https://t.me/lkcamp/15252)

**Mentores**:

- Pedro Sader Azevedo ([@pe_sader](https://t.me/pe_sader))
- Ícaro Chiabai ([@archicarus](https://t.me/archicarus))

---

# Ricing

*Ricing* é o ato customizar a aparência do seu ambiente de trabalho para
maximizar o seu valor estético. Sistemas Linux fornecem uma liberdade imensa
nesse quesito, de tal modo que usuários podem desenvolver ambientes que, mais
do que serem esteticamente agradáveis, também intensificam a produtividade, uma
vez que podem ser adaptados a uma variedade de fluxos de trabalho.

**Tecnologias:** ambientes desktop, gerenciadores de janela, Wayland/Xorg, Arch Linux.

**Modelo de contribuição**: compartilhamento de *screenshots* em fóruns 
([r/unixporn](https://www.reddit.com/r/unixporn)), compartilhamento de *dotfiles* em
git forges (GitHub, GitLab, Codeberg, etc).

**Comunicação do projeto**: Grupo de Telegram do LKCAMP

**Tópico no Telegram do LKCAMP**: [\[GA\] Rice](https://t.me/lkcamp/15266)

**Mentores**:

- Enzo ([@zpackss](https://t.me/zpackss))
- Ícaro Chiabai ([@archicarus](https://t.me/archicarus))
- Julio Nunes ([@jn513](https://t.me/jn513))
- Tárik Sá ([@lambdcalculus](https://t.me/lambdcalculus))
- Wallyson Oliveira ([@Sholum](https://t.me/Sholum))
