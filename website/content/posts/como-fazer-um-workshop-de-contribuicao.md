---
title: "Como fazer um evento de contribuição open source"
date: 2023-03-17T21:59:05-03:00
draft: false
categories: ["Documentação"]
tags: ["LHC", "Tosconf"]
authors: ["Pedro Sader Azevedo"]
---

Nos últimos tempos, realizamos vários eventos de contribuição para projetos de código aberto (ex: Hackathon de kUnit, Hackathon de Tradução, Hackathon de 1 Caractere, Calourada LKCAMP, etc). Por isso, decidimos documentar nosso método para organizar esse tipo de evento. Isso está separado nos tópicos abaixo e, para cada um dos quais, há um resumo ("TLDR").

* [Antes do evento](#antes-do-evento)
  * [Determinar o público alvo](#determinar-o-público-alvo)
  * [Escolher a contribuição](#escolher-a-contribuição)
  * [Selecionar projetos](#selecionar-projetos)
  * [Praticar a contribuição](#praticar-a-contribuição)
  * [Aprender e ensinar além do mínimo](#aprender-e-ensinar-além-do-mínimo)
  * [Escrever uma postagem no blog](#escrever-uma-postagem-no-blog)
* [Durante o evento](#durante-o-evento)
  * [Apresentar um tutorial](#apresentar-um-tutorial)
  * [Disponibilizar tempo para contribuir](#disponibilizar-tempo-para-contribuir)

---

## Antes do evento

### Determinar o público alvo

{{< alerts info
"**TLDR:** oriente-se pelos interesses e conhecimentos do público"
>}}

Antes de escolher a contribuição que será feita no evento é importante determinar o público alvo do evento. Isso é crucial para pensar em uma atividade que seja interessante e paupável para a maioria dos participantes esperados para o evento.

Por exemplo, quando fomos convidados a fazer um evento para a Semana da Computação da Unicamp (SECOMP), decidimos dedicar grande parte do evento ao ensinamento dos básicos de `git` e GitHub/GitLab. Isto, pois tínhamos a expectativa de que grande parte do público não estaria familiarizado com essas ferramentas, já que grande parte do público da SECOMP é composto por estudantes de outros curso além de Engenharia e Ciência da Computação.

### Escolher a contribuição

{{< alerts info
"**TLDR:** escolha uma contribuição que seja comum e siga um padrão"
>}}

{{< alerts warning
"**Ferramentas:** Telegram (comunicação entre organizadores do evento), Jitsi (serviço de video-conferência), Etherpad (registro)"
>}}

Ao determinar o público, o próximo passo é selecionar uma contribuição. Para isso, sempre buscamos uma mudança que:

1. Seja comum, para que possamos encontrar projetos que precisam dela
2. Siga um padrão, para que possamos ensiná-la em pouco tempo

Um exemplo desse tipo de contribuição foi a que fizemos no Hackathon de Tradução. Neste evento, ensinamos a contribuição com tradução para projetos que implementam essa funcionalidade usando uma tecnologia específica (GNU `gettext`). Essa contribuição é comum (1.), porque muitos projetos ainda carecem de tradução para o português brasileiro, e segue um padrão (2.), porque adicionar uma nova tradução segue o mesmo fluxo de trabalho mesmo entre diferentes projetos.

Tendo escolhido a contribuição, decidimos a sequência das atividades que serão realizadas no evento. Em seguida, dividimos as tarefas entre os membros do nosso grupo, deixando a responsabilidade de cada um escrita em um documento [Etherpad](https://etherpad.wikimedia.org/p/r.0a3559ff6acd281abe4e803394f22d77).

### Selecionar projetos

{{< alerts info
"**TLDR:** tenha uma lista de projetos pronta para o dia do evento"
>}}

{{< alerts warning
"**Ferramentas:** Ethercalc (planilhas), IRC ou Matrix ou Email (comunicação com mantenedores)"
>}}

Para que o evento aconteça de maneira fluida, nós preparamos uma planilha [Ethercalc](https://ethercalc.net/) com projetos que carecem da contribuição escolhida para o evento. Essa foi a planilha do Hackathon de 1 Caractere:

![planilha-projetos](/imgs/posts/como-fazer-um-workshop-de-contribuicao/planilha-projetos.png)

Essas planilhas tipicamente incluem os seguintes campos:

- Nome do projeto
- Descrição do projeto
- Link do repositório do projeto
- Screenshot do projeto
- Status: pull request, merged, etc
- Pessoa ou grupo responsável (inicialmente vazio)

Além disso, entramos em contato com os mantenedores dos projetos caso a convenção seja se comunicar antes de enviar uma contribuição (ex: por *issues* no GitHub ou GitLab, por mensagens no IRC ou Matrix, por emails em *mailing lists*, etc).

Isso ocorreu no Hackathon de kUnit, em que contribuimos para o kernel linux modernizando testes unitários. Levando em conta a escala do kernel linux, decidimos entrar em contato com os mantenedores das partes do kernel que receberiam as contribuições, a fim de melhor entender se a contribuição seria desejável para eles.

### Praticar a contribuição

{{< alerts info
"**TLDR:** toda a equipe deve fazer uma contribuição de exemplo"
>}}

{{< alerts warning
"**Ferramentas:** Ethercalc (planilhas)"
>}}


Depois de selecionar a contribuição e os projetos que carecem dela, todos os membros do LKCAMP envolvidos na organização do evento devem fazer uma contribuição. Com isso,

- Nós confeccionamos exemplos para servirem de modelo para os participantes do evento

- Nós antecipamos e nos preparamos para problemas comuns, que podem ocorrer no dia do evento (ex: problemas de setup, erros de compilação, etc)

Ao finalizar uma contribuição de exemplo, atualizamos a planilha de projetos, preenchendo o campo "Pessoa ou grupo responsável" com "Equipe LKCAMP" como destacado na imagem abaixo.

![planilha-lkcamp](/imgs/posts/como-fazer-um-workshop-de-contribuicao/planilha-lkcamp.png)


### Aprender e ensinar além do mínimo

{{< alerts info
"**TLDR:** aproveite evento para aprender e ensinar o máximo possível"
>}}

Para nós, os eventos são uma oportunidade de ensinar aos participantes tudo que julgarmos relevante para a contribuição escolhida. Por isso, sempre buscamos entender e explicar a contribuição muito além do mínimo necessário para fazê-la.

Por exemplo, a contribuição que ensinamos no Hackathon de 1 Caractere era muito simples (literamente um único caractere!), mas aproveitamos a oportunidade para explicar conceitos de programação orientada a objetos e noções de design de interface acessível.

### Escrever uma postagem no blog

{{< alerts info
"**TLDR:** material escrito é crucial para participantes e organizadores"
>}}

{{< alerts warning
"**Ferramentas:** Hugo (geração de site estático), GitLab (hospedagem)"
>}}

Por fim, confeccionamos um material escrito que documenta todo o processo de realização da contribuição escolhida (incluindo os conteúdos adicionais mencionados no item anterior). Esse material é publicada no nosso [blog](/posts).

Esse material serve como consulta para os participantes do evento, evitando que eles se percam durante nossas explicações. Além disso, esse material serve como documentação para a própria equipe do LKCAMP.

Vale dizer também que o formato escrito evita erros de digitação e de sintaxe, pois permite que os participantes copiem (`Ctrl + c`) e colem (`Ctrl + v`) os recortes de código exemplificados, ao invés de transcrevê-los manualmente a partir de slides.

---

## Durante o evento

### Apresentar um tutorial

{{< alerts info
"**TLDR:** a primeira parte do evento é curta e expositiva"
>}}

{{< alerts warning
"**Ferramentas:** Libre Office Impress ou LaTeX Beamer (slides)"
>}}

A primeira parte do evento é uma apresentação de slides, feita utilizando Libre Office Impress ou LaTeX Beamer, que ensina a contribuição escolhida e aborda os assuntos de aprofundamento relacionados a ela. Sempre que possível, a apresentação segue a mesma estrutura que a postagem no blog.

### Disponibilizar tempo para contribuir

{{< alerts info
"**TLDR:** a segunda parte do evento é longa e \"mão na massa\""
>}}

Depois da apresentação, a maior parte do tempo do evento é alocada para tempo livre para realizar a contribuição. Nesse momento, todos os membros do LKCAMP que estiverem presentes assumem o papel de monitores e prestam assistência individual aos participantes que ficarem com dúvidas.

---

Obrigado por ler até aqui, esperamos que essa postagem tenha sido informativa! Caso tenha interesse em organizar eventos de contribuição para projetos de código aberto, não hesite em entrar em contato com a gente no nosso [grupo de Telegram](https://t.me/lkcamp_unicamp).

Até a próxima! :penguin: :blue_heart:
