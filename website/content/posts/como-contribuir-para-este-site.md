---
title: "Como contribuir para este site"
date: 2022-04-27
draft: false
tags: ["Comunidade", "Website", "Meta"]
categories: ["Tutorial"]
authors: ["Pedro Sader Azevedo", "Ícaro Chiabai"]
---

Como tudo que fazemos no LKCAMP, este site tem código aberto e livre! Isso significa que você pode contribuir para esta página, corrigindo problemas, fazendo postagens no blog, etc.

Nesse tutorial, vou ensinar a fazer uma postagem (como essa!) no blog.

# Passo 1: Bifurcar o repositório

No LKCAMP, usamos o GitLab (alternativa livre ao GitHub) para armazenar repositórios `git` que contém nossas apresentações, sites, documentação, scripts, etc.

O primeiro passo para contribuir para o site é obter uma cópia do seu código na sua própria conta do GitLab. Chamamos essa cópia de fork (ou "bifurcação"), e fazê-la costuma ser o primeiro passo para contribuir para muitos projetos de código aberto!

Para isso, siga o link https://gitlab.com/lkcamp/lkcamp.gitlab.io e faça login na sua conta do GitLab. Em seguida, clique em "Fork".

![](/imgs/posts/como-contribuir-para-este-site/tutorial01.png)

Em seguida, escolha a visibilidade pública.

![](/imgs/posts/como-contribuir-para-este-site/tutorial02.png)

Agora, confirme a bifurcação clicando em "Fork the project". Isso vai te levar a uma página do GitLab que contém o seu fork deste site. Note que o link da página contém seu nome de usuário antes do nome do repositório, ao invés do nome "lkcamp", indicando que o repositório que você está vendo pertence a sua própria conta do GitLab.

> **Link do repositório original**
>
> [https://gitlab.com/**lkcamp**/lkcamp.gitlab.io](https://gitlab.com/lkcamp/lkcamp.gitlab.io)

> **Link do meu fork**
>
> [https://gitlab.com/**PeAzevedo**/lkcamp.gitlab.io](https://gitlab.com/PeAzevedo/lkcamp.gitlab.io)

Uma nomenclatura comum o repositório original é *upstream*, e, para o repositório bifurcado, *downstream*. O diagrama abaixo resume o que fizemos até agora:

![](/imgs/posts/como-contribuir-para-este-site/contribuicao01.png)

Agora que terminamos isso, vamos à próxima etapa!

## Passo 2: Obter uma cópia local do repositório

Para editar o código, é necessário baixá-lo em seu computador (a partir do repositório *downstream* que acabamos de criar). Vamos fazer isso usando o terminal, com a seguinte linha de comando:

{{< highlight bash >}}
git clone https://gitlab.com/PeAzevedo/lkcamp.gitlab.io
{{< / highlight >}}

No seu caso, substitua "PeAzevedo" pelo seu próprio nome de usuário.

Isso vai criar uma pasta com o nome `lkcamp.gitlab.io`, com todo o conteúdo do repositório! Podemos entrar nessa pasta pelo terminal, usando o comando `cd` (de "change directory"):

{{< highlight bash >}}
cd lkcamp.gitlab.io
{{< / highlight >}}

O site do LKCAMP usa um tema que está hospedado em um repositório separado, então é necessário copiá-lo para a pasta local. Felizmente incluimos o tema como um "submódulo", então o processo é inteiramente automatizado pelo comando:

{{< highlight bash >}}
git submodule update --init --recursive
{{< / highlight >}}

Estamos quase prontos para colocar a mão na massa! Mas antes, vale avisar que não é uma boa prática editar o código diretamente no galho principal do repositório, usualmente nomeado "master" ou "main". Você pode ver em que galho você está, entre outras informações importantes, com o comando:

{{< highlight bash >}}
git status
{{< / highlight >}}

Enfim, é mais seguro (e educado) fazer suas mudanças em um galho separado, que você pode nomear conforme o tema da mudança que você está buscando fazer. Para criar um galho e "entrar" nele, use os seguintes comandos:

{{< highlight bash >}}
git branch mybranch # cria o galho
git checkout mybranch # entra no galho
{{< / highlight >}}

O diagrama atualizado mostra o que fizemos até agora:

![](/imgs/posts/como-contribuir-para-este-site/contribuicao02.png)

# Passo 3: Colocar a mão na massa

Finalmente!

O site do LKCAMP usa um gerador de sites estáticos chamado Hugo, baseado na linguagem de programação Go. Felizmente, o Hugo cuida do layout (html), do estilo (css), e da lógica (javascript) da nossa página então podemos nos concentrar no conteúdo.

O Hugo usa arquivos de markdown para gerar os posts. Isso é uma enorme conveniência, porque a sintaxe do markdown é simples, legível, e fácil de aprender. Você pode aprendê-la usando esse [guia](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

Além das funcionalidades-padrão do markdown, o site do LKCAMP permite que você inclua alertas em suas postagens. Isso é bastante útil para destacar informações em sua postagem.

A aparência dos alertas é a seguinte:

- `error`

{{< alerts error
"**Cuidado:** jamais rode comandos de terminal que você não entende!"
>}}

- `warning`

{{< alerts warning
"**Atenção:** alterar o galho principal diretamente é uma má prática!"
>}}

- `info`

{{< alerts info
"**Aviso:** você clonou o repositório com sucesso!"
>}}


- `success`

{{< alerts success
"**Parabéns,** você viu todos os alertas!"
>}}

Você pode conferir a sintaxe dos alertas (e desse post inteiro) nesse [link](https://gitlab.com/PeAzevedo/lkcamp.gitlab.io/-/blob/master/content/posts/como-contribuir-para-este-site.md).

Enquanto você edita o site, você pode acessá-lo localmente para ver a aparência das suas mudanças. É necessário instalar o Hugo no seu computador para fazer isso, o que você pode resolver com um dos seguintes comandos, dependendo da sua distro:

{{< highlight bash >}}
sudo apt install hugo # Ubuntu, Debian, Linux Mint, Zorin OS, Pop!_OS, etc
sudo dnf install hugo # Fedora, CentOS, RHEL, etc
sudo pacman -Syu hugo # Arch Linux, Manjaro, Endeavor OS, etc
{{< / highlight >}}

Depois de instalado, o primeiro passo para fazer uma nova postagem no blog é criar um arquivo com extensão `.md` na pasta "posts" ou "events", que ficam dentro da pasta "content". Você pode fazer isso como quiser, mas recomendamos fortemente que seja feito usando o seguinte comando:

{{< highlight bash >}}
# rode diretamente da base do diretório do seu fork!
hugo new posts/novo_post.md     # para novas postagens
hugo new events/novo_evento.md  # para novos eventos
{{< / highlight >}}

Para vermos as mudanças que estamos realizando no site, devemos instanciar um
servidor do Hugo em nossa máquina utilizando o comando abaixo:

{{< highlight bash >}}
# rode diretamente da base do diretório do seu fork!
hugo server
{{< / highlight >}}

E acessar o link impresso na tela:

![](/imgs/posts/como-contribuir-para-este-site/tutorial03.png)

Nesse link você acessará uma versão do site que contém as suas alterações,
assim podendo verificar o funcionamento e aparência da mesma. Caso suas
mudanças comprometam o funcionamento do site, esse link vai mostrar uma
mensagem de erro para ajudar a debuggar seu código. O tema do site que
utilizamos é um submódulo, isto é, é um outro repositório. Se quiser contribuir
para o nosso [tema](https://gitlab.com/lkcamp/lkcamp-hugo-theme), você deverá
repetir os processos citados nesse tutorial, mas para os arquivos do tema.

Quando você terminar de alterar o código (e de garantir que o site continua funcionando), siga para próximo passo!

# Passo 4: Empurrar suas mudanças para o dowstream

O que você deve fazer agora é adicionar as mudanças que fez ao "histórico" do repositório. Para isso, você deve descobrir quais arquivos você editou, usando o (já conhecido) comando:

{{< highlight bash >}}
git status
{{< / highlight >}}

Se quiser conferir as mudanças que você fez linha por linha, o comando é:

{{< highlight bash >}}
git diff
{{< / highlight >}}

Feito isso, adicione os arquivos alterados usando o comando `git add caminho/para/arquivo`. Como estamos fazendo um exemplo de postagem no blog, esse comando vai ser mais ou menos assim:

{{< highlight bash >}}
git add content/posts/titulo-da-postagem.md
{{< / highlight >}}

Agora, você indica ao `git` que se "compromete" com as mudanças que fez, para que o `git` as adicione ao histórico do repositório. O comando para fazer isso é:

{{< highlight bash >}}
git commit -m "mensagem de commit" # commit significa "comprometer" em inglês!
{{< / highlight >}}

Na mensagem de commit você deve incluir um resumo bem breve da sua mudança, como se fosse um título para ela. No caso de uma postagem nova, uma possível mensagem de commit seria "adicionar postagem sobre contribuição para o site".

O detalhe é que essa mudança foi adicionada apenas ao histórico do galho "novo_post"! Para adicionar essa mudança ao histórico galho principal, você deve fundir os dois galhos. Primeiro, volte ao galho principal:

{{< highlight bash >}}
git checkout master # se o galho principal chama "master"
git checkout main # se o galho principal chama "main"
{{< / highlight >}}

Você pode verificar a "mágica do `git`" nesse momento, conferindo que o arquivo que você criou e commitou no outro galho ainda não está presente no galho principal! Mas não se preocupe, vamos arrumar isso com esse comando:

{{< highlight bash >}}
git merge novo_post
{{< / highlight >}}

Por fim, vamos atualizar o nosso repositório bifurcado (o *downstream*) com as mudanças que fizemos. Esse processo de atualizar os repositórios remotos com as mudanças feitas em repositórios locais é chamado de "empurrar" (ou "push" e, inglês). O comando para fazer isso é:

{{< highlight bash >}}
git push master # se o galho principal chama "master"
git push main # se o galho principal chama "main"
{{< / highlight >}}

Assim fica o diagrama do fluxo de contribuição atualizado.

![](/imgs/posts/como-contribuir-para-este-site/contribuicao03.png)

# Passo 5: Submeter suas mudanças ao upstream

O último passo vai ser pedir para os admnistradores do site do LKCAMP aceitarem sua mudança. Para isso, entre no link do seu repositório *downstream* e clique no botão com um "+", no meio da tela, depois em "New merge request".

![](/imgs/posts/como-contribuir-para-este-site/tutorial04.png)

Em seguida, escolha o galho principal do seu repositório *downstream* como código fonte da mudança.

![](/imgs/posts/como-contribuir-para-este-site/tutorial05.png)

Finalize o processo dando um título e escrevendo uma descrição para a sua mudança. Isso conclui o processo de contribuição e completa nosso diagrama (com a setinha do "New merge request")!

![](/imgs/posts/como-contribuir-para-este-site/contribuicao04.png)

Quando o pessoal do LKCAMP aceitar, rejeitar, ou dar feedback sobre sua contribuição, você vai ser notificado por email.

Agora é só esperar :wink:.

# Passo 6: Contribuir novamente

Quando você quiser contribuir novamente, não precisa repetir os primeiros dois passos! O único cuidado que você deve tomar é que, com o tempo, o seu repositório *downstream* vai ficando desatualizado conforme o repositório *upstream* muda (por exemplo, aceitando contribuições de outras pessoas). 

Para resolver isso, você deve primeiro adicionar uma referência ao repositório *upstream* à sua cópia local:

{{< highlight bash >}}
git remote add upstream https://gitlab.com/lkcamp/lkcamp.gitlab.io
{{< / highlight >}}

Feito isso, você deve periodicamente atualizar a sua cópia local com as mudanças do *upstream*. Esse processo de atualizar o seu repositório local com as mudanças feitas em um repositório remoto é chamada de "puxar" (ou "pull" em inglês). O comando para isso é:

{{< highlight bash >}}
git pull upstream master
{{< / highlight >}}

Por fim, empurre sua cópia local atualizada para o seu repositório *downstream*:

{{< highlight bash >}}
git push origin master
{{< / highlight >}}

E, para não perder o costume, aí vai um diagrama desse processo:

![](/imgs/posts/como-contribuir-para-este-site/contribuicao05.png)

Isso encerra o tutorial! Se você quiser adicionar alguma informação, melhorar alguma explicação, ou corrigir algum erro gramatical, sua contribuição será muito bem vinda :grin:

Até a próxima!
