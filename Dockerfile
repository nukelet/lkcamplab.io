FROM nginx:mainline-alpine

WORKDIR /lkcamp-website
COPY . .

# Remove the default nginx configs
RUN rm /etc/nginx/conf.d/*;
# Copy our custom nginx config
RUN cp nginx/*.conf /etc/nginx/conf.d/;

# Install hugo
RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo;
# Generate the static files for the main website
RUN hugo -b / -s website -d /var/www/website;
# Generate the static files for each entry in `blogs` to /var/www/blogs/{entry1,entry2,...}
RUN mkdir /var/www/members && \
  for dir in ./members/*/; do hugo -b /$dir -s $dir -d /var/www/$dir; done;

# Start Nginx
# reference: https://github.com/nginxinc/docker-nginx/blob/master/Dockerfile-debian.template
CMD ["nginx", "-g", "daemon off;"]
